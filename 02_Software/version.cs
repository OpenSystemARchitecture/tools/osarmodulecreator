/******************************************************************************
 * @file      version.cs
 * @author  
 * @proj      OsarModuleCreator
 * @date      Saturday, December 5, 2020
 * @version   Application v. 1.0.1.2
 * @version   Generator   v. 1.2.3.9
 * @brief     Controls the assembly Version
 *****************************************************************************/
using System;
using System.Reflection;

[assembly: AssemblyVersion("1.0.1.2")]

namespace OsarModuleCreator
{
  static public class OsarModuleCreatorVersionClass
	{
		public static int major { get; set; }	 //Version of the programm
		public static int minor { get; set; }	 //Sub version of the programm
		public static int patch { get; set; }	 //Debug patch of the orgramme
		public static int build { get; set; }	 //Count programm builds
		
    static OsarModuleCreatorVersionClass()
    {
			major = 1;
			minor = 0;
			patch = 1;
			build = 2;
    }

		public static string getCurrentVersion()
    {
      return major.ToString() + '.' + minor.ToString() + '.' + patch.ToString() + '.' + build.ToString();
    }
	}
}
//!< @version 0.1.0	->	Inital creation of the OSAR module creator.
//!< @version 0.1.1	->	Adding template mem map file generation.
//!< @version 0.1.2	->	Adding generation of low level interface header
//!< @version 0.1.3	->	Add generation of an generic interface header file and an high level interface header file.
//!< @version 0.1.4	->	Bugfix of spelling errors
//!< @version 0.1.5	->	Add an additional error type 
//!< @version 0.2.0	->	Adding generation of a module makefile
//!< @version 0.2.2	->	Adapt generation of "Std_Types.h" Header file include generation.
//!< @version 0.2.3	->	Adapt standard generation settings.
//!< @version 1.0.0	->	Adding generation of User Code Areas and UUIDs
//!< @version 1.0.1	->	Adding generation support of doxygen master group tag
