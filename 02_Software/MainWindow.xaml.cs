﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Threading;
using OsarResources.Generator.Resources;
namespace OsarModuleCreator
{
  /// <summary>
  /// Interaktionslogik für MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {

    Thread generatorThread;
    
    public MainWindow()
    {
      InitializeComponent();

      // Instantiate the writer 
      TextWriter _writer = new ConsoleWriter(osarGenerationStatus);
      // Redirect the out Console stream 
      Console.SetOut(_writer);
      Console.WriteLine("> Start OSAR module creator.");
    }

    private void osarButtonSelectPath_Click(object sender, RoutedEventArgs e)
    {
      FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

      if (System.Windows.Forms.DialogResult.OK == folderBrowserDialog1.ShowDialog())
      {
        osarModuleBasePath.Text = folderBrowserDialog1.SelectedPath;
      }
    }

    /* Start of Generaton */
    private void osarGenerateFiles_Click(object sender, RoutedEventArgs e)
    {
      /* Setup elements */
      OsarModuleGenerator.BasePath = osarModuleBasePath.Text;
      OsarModuleGenerator.ModuleName = osarModuleName.Text;
      OsarModuleGenerator.AuthorName = osarModuleAuthor.Text;
      OsarModuleGenerator.DetModuleId = osarDetModuleId.Text;

      OsarModuleGenerator.CreateInitMemoryFunction = osarCbCreateInitMemoryFnc.IsChecked.Value;
      OsarModuleGenerator.CreateMainfunctionFunction = osarCbCreateMainfunctionFnc.IsChecked.Value;
      OsarModuleGenerator.UseDetModule = osarCbCreateDetElements.IsChecked.Value;
      OsarModuleGenerator.GenerateDetIntoStaticFile = osarDetStaticFileGeneration.IsChecked.Value;
      OsarModuleGenerator.CreateAnStaticCfgFile = osarCreateStaticCfgFile.IsChecked.Value;
      OsarModuleGenerator.CreateDummyGenFiles = osarCreateDummyGenCfgFile.IsChecked.Value;
      OsarModuleGenerator.UseOsarMemMap = osarCbCreateMemoryMapping.IsChecked.Value;
      OsarModuleGenerator.CreateTypesFile = osarCbCreateTypesFile.IsChecked.Value;
      OsarModuleGenerator.CreateLowLevelIfFile = osarCbCreateLowLevelIfFile.IsChecked.Value;
      OsarModuleGenerator.CreateHighLevelIfFile = osarCbCreateHighLevelIfFile.IsChecked.Value;
      OsarModuleGenerator.CreateGenericIfFile = osarCbCreateGenericIfFile.IsChecked.Value;
      OsarModuleGenerator.CreateUserCodeSections = osarCbCreateUserCodeSectionAreas.IsChecked.Value;
      OsarModuleGenerator.CreateUUIDs = osarCbCreateUUIDs.IsChecked.Value;

      OsarModuleGenerator.CreateCreateModuleMakefile = osarCbCreateModuleMakefileFile.IsChecked.Value;
      OsarModuleGenerator.MakefileModuleBaseFolder = osarTbMakefileBaseFolder.Text;


      /* Evaluate Combo box */
      switch (osarCombModuleMasterTypeSelection.SelectedIndex)
      {
        case 0:
          OsarModuleGenerator.ModuleMasterGroup = "";
        break;

        case 1:
        OsarModuleGenerator.ModuleMasterGroup = osarTbModuleMasterType.Text;
        break;

        case 2:
          OsarModuleGenerator.ModuleMasterGroup = genericSource.DoxygenOsarMasterGroupSWC;
        break;

        case 3:
          OsarModuleGenerator.ModuleMasterGroup = genericSource.DoxygenOsarMasterGroupBSW;
        break;

        case 4:
          OsarModuleGenerator.ModuleMasterGroup = genericSource.DoxygenOsarMasterGroupCD;
        break;

        case 5:
          OsarModuleGenerator.ModuleMasterGroup = genericSource.DoxygenOsarMasterGroupCDD;
        break;

        case 6:
          OsarModuleGenerator.ModuleMasterGroup = genericSource.DoxygenOsarMasterGroupMCAL_STM32F4Cube;
        break;

        default:
          OsarModuleGenerator.ModuleMasterGroup = "";
        break;
      }



      //Task.Run(() => moduleGenerator.GenerateElements());

      if (generatorThread != null)
      {
        generatorThread.Abort();
        generatorThread = null;
      }

      generatorThread = new Thread(OsarModuleGenerator.GenerateElements);
      generatorThread.Start();


    }

    /* Exit program */
    private void osarExit_Click(object sender, RoutedEventArgs e)
    {
      Environment.Exit(0);
    }
  }
}
