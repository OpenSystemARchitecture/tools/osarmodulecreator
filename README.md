# OsarModuleCreator

## General
Tool to create an OSAR conform embedded software module.

Tool allows the generation of an complete initial "C" source code module. Including basic scheduling interfaces, data type files, make-files and more.

## Abbreviations:
OSAR == Open System ARchitecture

## Useful Links:
- Overall OSAR-Artifactory: http://riddiks.ddns.net:8082/artifactory/webapp/#/artifacts/browse/tree/General/prj-open-system-architecture
- OSAR - ModuleCreator releases: http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Tools/OSAR_ModuleCreator
- OSAR - Documents: http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Documents/