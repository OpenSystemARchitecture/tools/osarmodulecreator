﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.ModuleMakefile;
using OsarSourceFileLib.DocumentationObjects.General;
using OsarResources.Generator.Resources;

namespace OsarModuleCreator
{
  static public class OsarModuleGenerator
  {
    static private string moduleName;
    static private string basePath;
    static private string authorName;
    static private bool createInitMemoryFunction;
    static private bool createMainfunctionFunction;
    static private bool useDetModule;
    static private bool generateDetIntoStaticFile;
    static private bool createAnStaticCfgFile;
    static private bool createDummyGenFiles;
    static private bool createTypesFile;
    static private bool createLowLevelIfFile;
    static private bool createHighLevelIfFile;
    static private bool createGenericIfFile;
    static private bool createModuleMakefile;
    static private bool createUserCodeSections;
    static private bool createUUIDs;
    static private string makefileModuleBaseFolder;
    static private bool useOsarMemMap;
    static private DoxygenFileGroupOpener moduleFileGroupOpener;
    static private DoxygenFileGroupCloser moduleFileGroupCloser;
    static private string detModuleId;
    static private CFileVariableObjects globalVarWithMemMap;
    static private CFileFunctionObjects globalFunctionWithMemMap;
    static private string moduleMasterGroup;
    static public string ModuleName
    {
      get
      {
        return moduleName;
      }

      set
      {
        moduleName = value;
      }
    }

    static public string BasePath
    {
      get
      {
        return basePath;
      }

      set
      {
        basePath = value;
      }
    }

    static public string AuthorName
    {
      get
      {
        return authorName;
      }

      set
      {
        authorName = value;
      }
    }

    static public bool CreateInitMemoryFunction
    {
      get
      {
        return createInitMemoryFunction;
      }

      set
      {
        createInitMemoryFunction = value;
      }
    }

    static public bool CreateMainfunctionFunction
    {
      get
      {
        return createMainfunctionFunction;
      }

      set
      {
        createMainfunctionFunction = value;
      }
    }

    static public bool UseDetModule
    {
      get
      {
        return useDetModule;
      }

      set
      {
        useDetModule = value;
      }
    }

    static public bool GenerateDetIntoStaticFile
    {
      get
      {
        return generateDetIntoStaticFile;
      }

      set
      {
        generateDetIntoStaticFile = value;
      }
    }

    static public bool CreateAnStaticCfgFile
    {
      get
      {
        return createAnStaticCfgFile;
      }

      set
      {
        createAnStaticCfgFile = value;
      }
    }

    static public bool CreateDummyGenFiles
    {
      get
      {
        return createDummyGenFiles;
      }

      set
      {
        createDummyGenFiles = value;
      }
    }

    static public bool UseOsarMemMap
    {
      get
      {
        return useOsarMemMap;
      }

      set
      {
        useOsarMemMap = value;
      }
    }

    public static string DetModuleId
    {
      get
      {
        return detModuleId;
      }

      set
      {
        detModuleId = value;
      }
    }

    public static bool CreateTypesFile
    {
      get
      {
        return createTypesFile;
      }

      set
      {
        createTypesFile = value;
      }
    }

    public static bool CreateLowLevelIfFile
    {
      get
      {
        return createLowLevelIfFile;
      }

      set
      {
        createLowLevelIfFile = value;
      }
    }

    public static bool CreateHighLevelIfFile
    {
      get
      {
        return createHighLevelIfFile;
      }

      set
      {
        createHighLevelIfFile = value;
      }
    }

    public static bool CreateGenericIfFile
    {
      get
      {
        return createGenericIfFile;
      }

      set
      {
        createGenericIfFile = value;
      }
    }

    public static bool CreateCreateModuleMakefile
    {
      get
      {
        return createModuleMakefile;
      }

      set
      {
        createModuleMakefile = value;
      }
    }

    public static string MakefileModuleBaseFolder
    {
      get
      {
        return makefileModuleBaseFolder;
      }

      set
      {
        makefileModuleBaseFolder = value;
      }
    }

    public static bool CreateUserCodeSections
    {
      get
      {
        return createUserCodeSections;
      }

      set
      {
        createUserCodeSections = value;
      }
    }

    public static bool CreateUUIDs
    {
      get
      {
        return createUUIDs;
      }

      set
      {
        createUUIDs = value;
      }
    }

    public static string ModuleMasterGroup
    {
      get
      {
        return moduleMasterGroup;
      }

      set
      {
        moduleMasterGroup = value;
      }
    }

    static void InitGenerator()
    {
      globalVarWithMemMap = new CFileVariableObjects();
      globalFunctionWithMemMap = new CFileFunctionObjects();

      GeneralStartGroupObject startMapGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject endMapGroup = new GeneralEndGroupObject();

      /* Map variables into constand memory. */
      startMapGroup.AddGroupName("Map variables into constand memory");
      startMapGroup.AddOsarStartMemMapDefine(moduleName, OsarMemMapDataType.OSAR_CONST);
      endMapGroup.AddOsarStopMemMapDefine(moduleName, OsarMemMapDataType.OSAR_CONST);
      globalVarWithMemMap.AddCFileObjectGroup(startMapGroup, endMapGroup);

      /* Map variables into uninitialized memory */
      startMapGroup.AddGroupName("Map variables into uninitialized memory");
      startMapGroup.AddOsarStartMemMapDefine(moduleName, OsarMemMapDataType.OSAR_NOINIT_VAR);
      endMapGroup.AddOsarStopMemMapDefine(moduleName, OsarMemMapDataType.OSAR_NOINIT_VAR);
      globalVarWithMemMap.AddCFileObjectGroup(startMapGroup, endMapGroup);

      /* Map variables into initialized memory */
      startMapGroup.AddGroupName("Map variables into initialized memory");
      startMapGroup.AddOsarStartMemMapDefine(moduleName, OsarMemMapDataType.OSAR_INIT_VAR);
      endMapGroup.AddOsarStopMemMapDefine(moduleName, OsarMemMapDataType.OSAR_INIT_VAR);
      globalVarWithMemMap.AddCFileObjectGroup(startMapGroup, endMapGroup);

      /* Map variables into zero initialized memory */
      startMapGroup.AddGroupName("Map variables into zero initialized memory");
      startMapGroup.AddOsarStartMemMapDefine(moduleName, OsarMemMapDataType.OSAR_ZERO_VAR);
      endMapGroup.AddOsarStopMemMapDefine(moduleName, OsarMemMapDataType.OSAR_ZERO_VAR);
      globalVarWithMemMap.AddCFileObjectGroup(startMapGroup, endMapGroup);

      /* Map function into code memory */
      startMapGroup.AddGroupName("Map function into code memory");
      startMapGroup.AddOsarStartMemMapDefine(moduleName, OsarMemMapDataType.OSAR_CODE);
      endMapGroup.AddOsarStopMemMapDefine(moduleName, OsarMemMapDataType.OSAR_CODE);
      globalFunctionWithMemMap.AddCFileObjectGroup(startMapGroup, endMapGroup);
    }

    static public void GenerateElements()
    {
      InitGenerator();
      /* >>>>> Create of folder structure <<<<< */
      /* Create of base folder */
      Console.WriteLine("> Start creating the base foder structure.");
      try
      {
        Directory.CreateDirectory(basePath + "\\" + moduleName + "\\01_Generator");
        Directory.CreateDirectory(basePath + "\\" + moduleName + "\\02_Mandatory\\inc");
        Directory.CreateDirectory(basePath + "\\" + moduleName + "\\02_Mandatory\\src");
        Directory.CreateDirectory(basePath + "\\" + moduleName + "\\03_GenData\\inc");
        Directory.CreateDirectory(basePath + "\\" + moduleName + "\\03_GenData\\src");
        Directory.CreateDirectory(basePath + "\\" + moduleName + "\\04_StaticData\\inc");
        Directory.CreateDirectory(basePath + "\\" + moduleName + "\\04_StaticData\\src");
        Directory.CreateDirectory(basePath + "\\" + moduleName + "\\05_Doc");
        Directory.CreateDirectory(basePath + "\\" + moduleName + "\\06_Makefile");

        moduleFileGroupOpener = new DoxygenFileGroupOpener();
        moduleFileGroupOpener.AddToDoxygenGroup(moduleName);

        /* Check usage of master group */
        if("" != moduleMasterGroup)
        {
          moduleFileGroupOpener.AddToDoxygenMasterGroup(moduleMasterGroup);
          moduleFileGroupCloser = new DoxygenFileGroupCloser(true);
        }
        else
        {
          moduleFileGroupCloser = new DoxygenFileGroupCloser(false);
        }


        if (true == createDummyGenFiles)
        {
          GenerateDummyGenFiles();
        }

        if (true == createAnStaticCfgFile)
        {
          GenerateStaticCfgFile();
        }

        GenerateModuleFiles();

        if (true == createTypesFile)
          GenerateTypesFile();

        if (true == useOsarMemMap)
          GenerateMemMapFile();

        if(true == createLowLevelIfFile)
        {
          GeneratLowLevelIfFile();
        }

        if(true == createHighLevelIfFile)
        {
          GeneratHighLevelIfFile();
        }

        if(true == createGenericIfFile)
        {
          GeneratGenericIfFile();
        }

        if (true == createModuleMakefile)
        {
          GenerateModuleMakefileFile();
        }

        Console.WriteLine("> Generation ENDs successful.\n\n");
      }
      catch (Exception ex)
      {
        Console.WriteLine("> An error occured >> msg: " + ex);
      }
    }


    /* Generate dummy gen files */
    static private void GenerateDummyGenFiles()
    {
      string fileNameHeader = moduleName + "_PBCfg.h";
      string fileNameSource = moduleName + "_PBCfg.c";
      CHeaderFile genHeader = new CHeaderFile();
      CSourceFile genSource = new CSourceFile();
      CFileIncludeObjects genHeaderIncludes = new CFileIncludeObjects();
      CFileIncludeObjects genSourceIncludes = new CFileIncludeObjects();
      DoxygenFileHeader doxHeader = new DoxygenFileHeader();
      CFileDefinitionObjects genDefinitions = new CFileDefinitionObjects();
      GeneralStartGroupObject startDetDefGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject endDetDefGroup = new GeneralEndGroupObject();

      /*------------------- Generate Dummy Header file ---------------------*/
      Console.WriteLine("> Start generation of dummy generated header file: " + fileNameHeader);
      doxHeader.AddFileAuthor(authorName);
      doxHeader.AddFileBrief("Generated header file data of the " + moduleName + " module.");
      doxHeader.AddFileName(fileNameHeader);
      doxHeader.AddFileDetails("Definition of general generated data.");
      doxHeader.AddFileNote(genericSource.CommentGeneratorDisclaimer);

      //Setup include files
      if (true == createTypesFile)
      {
        genHeaderIncludes.AddIncludeFile(moduleName + "_Types.h");
      }
      else
      {
        genHeaderIncludes.AddIncludeFile("Std_Types.h");
      }

      //Check if Det infomrations shall be generated here
      if (( false == generateDetIntoStaticFile ) && ( true == useDetModule ))
      {
        int groupidx;
        startDetDefGroup.AddGroupName(" Development error trace information for this module");
        groupidx = genDefinitions.AddCFileObjectGroup(startDetDefGroup, endDetDefGroup);
        genDefinitions.AddDefinition(( moduleName.ToUpper() + "_DET_MODULE_ID" ), detModuleId, groupidx);
        genDefinitions.AddDefinition(( moduleName.ToUpper() + "_MODULE_USE_DET" ), "STD_ON", groupidx);
      }

      
      genHeader.AddFileName(fileNameHeader);
      genHeader.AddFilePath(basePath + "\\" + moduleName + "\\03_GenData\\inc");
      genHeader.AddFileGroup(moduleFileGroupOpener, moduleFileGroupCloser);
      genHeader.AddFileCommentHeader(doxHeader);
      genHeader.AddIncludeObject(genHeaderIncludes);
      genHeader.AddDefinitionObject(genDefinitions);
      genHeader.GenerateSourceFile();


      /*------------------- Generate Dummy Source file ---------------------*/
      Console.WriteLine("> Start generation of dummy generated header file: " + fileNameSource);

      doxHeader.AddFileName(fileNameSource);
      doxHeader.AddFileBrief("Generated source file data of the " + moduleName + " module.");
      doxHeader.AddFileDetails("Implementation of general generated data.");

      //Setup includes
      if (true == useDetModule)
      {
        genSourceIncludes.AddIncludeFile("Det.h");
      }
      genSourceIncludes.AddIncludeFile(fileNameHeader);

      /* Gen mem map defines if needed */
      if (true == useOsarMemMap)
      {
        genSource.AddPrivateVariableObject(globalVarWithMemMap);
        genSource.AddGlobalVariableObject(globalVarWithMemMap);
        genSource.AddPrivateFunctionObject(globalFunctionWithMemMap);
        genSource.AddGlobalFunctionObject(globalFunctionWithMemMap);
      }

      genSource.AddFileName(fileNameSource);
      genSource.AddFilePath(basePath + "\\" + moduleName + "\\03_GenData\\src");
      genSource.AddFileCommentHeader(doxHeader);
      genSource.AddIncludeObject(genSourceIncludes);
      genSource.AddFileGroup(moduleFileGroupOpener, moduleFileGroupCloser);
      genSource.GenerateSourceFile();
    }

    /* Generation of Configuration file */
    static private void GenerateStaticCfgFile()
    {
      string fileNameHeader = moduleName + "_Cfg.h";
      CHeaderFile genHeader = new CHeaderFile();
      CFileIncludeObjects genHeaderIncludes = new CFileIncludeObjects();
      DoxygenFileHeader doxHeader = new DoxygenFileHeader();
      CFileDefinitionObjects genDefinitions = new CFileDefinitionObjects();
      GeneralStartGroupObject startDetDefGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject endDetDefGroup = new GeneralEndGroupObject();

      /*--------------------------------------------------------------------------------------------------------------*/
      Console.WriteLine("> Start generation of static configuration header file: " + fileNameHeader);
      doxHeader.AddFileAuthor(authorName);
      doxHeader.AddFileBrief("Generic configuration parameters of the " + moduleName + " module.");
      doxHeader.AddFileName(fileNameHeader);
      doxHeader.AddFileDetails("Definition of general configuration parameters and values.");
      doxHeader.AddFileNote(genericSource.CommentGeneratorDisclaimer);

      //Setup include files
      if (true == createTypesFile)
      {
        genHeaderIncludes.AddIncludeFile(moduleName + "_Types.h");
      }
      else
      {
        genHeaderIncludes.AddIncludeFile("Std_Types.h");
      }

      //Check if Det infomrations shall be generated here
      if ( (true == generateDetIntoStaticFile) && ( true == useDetModule ))
      {
        int groupidx;
        startDetDefGroup.AddGroupName(" Development error trace information for this module");
        groupidx = genDefinitions.AddCFileObjectGroup(startDetDefGroup, endDetDefGroup);
        genDefinitions.AddDefinition(( moduleName.ToUpper() + "_DET_MODULE_ID" ), detModuleId, groupidx);
        genDefinitions.AddDefinition(( moduleName.ToUpper() + "_MODULE_USE_DET" ), "STD_ON", groupidx);
      }


      genHeader.AddFileName(fileNameHeader);
      genHeader.AddFilePath(basePath + "\\" + moduleName + "\\04_StaticData\\inc");
      genHeader.AddFileCommentHeader(doxHeader);
      genHeader.AddFileGroup(moduleFileGroupOpener, moduleFileGroupCloser);
      genHeader.AddIncludeObject(genHeaderIncludes);
      genHeader.AddDefinitionObject(genDefinitions);
      genHeader.GenerateSourceFile();
    }

    /* Generation of basic module files */
    static private void GenerateModuleFiles()
    {
      string fileNameHeader = moduleName + ".h";
      string fileNameSource = moduleName + ".c";
      CHeaderFile genHeader = new CHeaderFile();
      CSourceFile genSource = new CSourceFile();
      DoxygenFileHeader doxHeader = new DoxygenFileHeader();
      CFileFunctionObjects functions = new CFileFunctionObjects();
      DoxygenElementDescription doxFunctionDesc = new DoxygenElementDescription();
      GeneralStartGroupObject startCodeMapGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject endCodeMapGroup = new GeneralEndGroupObject();
      CFileIncludeObjects includesSource = new CFileIncludeObjects();
      CFileIncludeObjects includesHeader = new CFileIncludeObjects();
      CFileTypeObjects headerTypes = new CFileTypeObjects();
      DoxygenElementDescription doxEnumDescr = new DoxygenElementDescription();
      int codeMapGroupId;

      /*==================================================================================================================================== */
      /* ------------------------------------------------------ Generate source file ------------------------------------------------------- */
      /*==================================================================================================================================== */
      Console.WriteLine("> Start generation of module source file: " + fileNameSource);
      doxHeader.AddFileAuthor(authorName);
      doxHeader.AddFileBrief("Implementation of functionalities from the \"" + moduleName + "\" module.\n");
      doxHeader.AddFileName(fileNameSource);
      doxHeader.AddFileDetails("TODO: Add module description.\n");
      doxHeader.AddFileNote(genericSource.CommentGeneratorDisclaimer);


      /* Set global doxygen Element description */
      doxFunctionDesc.AddElementReturnValues("None");
      doxFunctionDesc.AddElementParameter("None", CParameterType.INPUT);

      //Check Osar Memory Mapping
      if (true == useOsarMemMap)
      {
        startCodeMapGroup.AddOsarStartMemMapDefine(moduleName, OsarMemMapDataType.OSAR_CODE);
        endCodeMapGroup.AddOsarStopMemMapDefine(moduleName, OsarMemMapDataType.OSAR_CODE);
        codeMapGroupId = functions.AddCFileObjectGroup(startCodeMapGroup, endCodeMapGroup);

        /*=============================================== Create functions =================================================================== */
        /* Create init memory function */
        if(true == createInitMemoryFunction)
        {
          doxFunctionDesc.AddElementBrief("Module global memory initialization function.");
          doxFunctionDesc.AddElementDetails("This function shall be called during system startup. And before calling the init and mainfuntion.");
          if(true == createUUIDs)
          { doxFunctionDesc.GenerateElementUUID(); }
          functions.AddFunction("void", moduleName + "_InitMemory", "void", codeMapGroupId, doxFunctionDesc);
        }

        /* Create init function */
        doxFunctionDesc.AddElementBrief("Module global initialization function.");
        doxFunctionDesc.AddElementDetails("This function shall be called before using the module and after the memory initialization.");
        if (true == createUUIDs)
        { doxFunctionDesc.GenerateElementUUID(); }
        functions.AddFunction("void", moduleName + "_Init", "void", codeMapGroupId, doxFunctionDesc);

        /* Create mainfunctin function */
        if (true == createMainfunctionFunction)
        {
          doxFunctionDesc.AddElementBrief("Module global mainfunction.");
          doxFunctionDesc.AddElementDetails("This function shall be called form the actual runtime environment within an fixed cycle.");
          if (true == createUUIDs)
          { doxFunctionDesc.GenerateElementUUID(); }
          functions.AddFunction("void", moduleName + "_Mainfunction", "void", codeMapGroupId, doxFunctionDesc);
        }

        /* Adding variables */
        genSource.AddGlobalVariableObject(globalVarWithMemMap);
        genSource.AddPrivateVariableObject(globalVarWithMemMap);

        /* Add private functions */
        genSource.AddPrivateFunctionObject(globalFunctionWithMemMap);
      }
      else
      {
        /*=============================================== Create functions =================================================================== */
        /* Create init memory function */
        if (true == createInitMemoryFunction)
        {
          doxFunctionDesc.AddElementBrief("Module global memory initialization function.");
          doxFunctionDesc.AddElementDetails("This function shall be called during system startup. And before calling the init and mainfuntion.");
          if (true == createUUIDs)
          { doxFunctionDesc.GenerateElementUUID(); }
          functions.AddFunction("void", moduleName + "_InitMemory", "void", doxFunctionDesc);
        }

        /* Create init function */
        doxFunctionDesc.AddElementBrief("Module global initialization function.");
        doxFunctionDesc.AddElementDetails("This function shall be called before using the module and after the memory initialization.");
        if (true == createUUIDs)
        { doxFunctionDesc.GenerateElementUUID(); }
        functions.AddFunction("void", moduleName + "_Init", "void", doxFunctionDesc);

        /* Create mainfunctin function */
        if (true == createMainfunctionFunction)
        {
          doxFunctionDesc.AddElementBrief("Module global mainfunction.");
          doxFunctionDesc.AddElementDetails("This function shall be called form the actual runtime environment within an fixed cycle.");
          if (true == createUUIDs)
          { doxFunctionDesc.GenerateElementUUID(); }
          functions.AddFunction("void", moduleName + "_Mainfunction", "void", doxFunctionDesc);
        }
      }

      /* Add includes */
      if (true == useDetModule)
      {
        includesSource.AddIncludeFile("Det.h");
      }
      includesSource.AddIncludeFile(fileNameHeader);

      /* Add Objects to gen source to generate file */
      genSource.AddFileName(fileNameSource);
      genSource.AddFilePath(basePath + "\\" + moduleName + "\\04_StaticData\\src");
      genSource.AddFileCommentHeader(doxHeader);
      genSource.AddFileGroup(moduleFileGroupOpener, moduleFileGroupCloser);
      genSource.AddGlobalFunctionObject(functions);
      genSource.AddIncludeObject(includesSource);

      if(true == createUserCodeSections)
      {
        genSource.AddUserCodeSections();
      }

      genSource.GenerateSourceFile();

      /*==================================================================================================================================== */
      /* ------------------------------------------------------ Generate header file ------------------------------------------------------- */
      /*==================================================================================================================================== */
      Console.WriteLine("> Start generation of module header file: " + fileNameHeader);

      /* Add doxygen Header */
      doxHeader.AddFileAuthor(authorName);
      doxHeader.AddFileBrief("Implementation of definitions / interface function prototypes / datatypes and generic module interface informations of the \"" + moduleName + "\" module.\n");
      doxHeader.AddFileName(fileNameHeader);
      doxHeader.AddFileDetails("TODO: Add module description.\n");
      doxHeader.AddFileNote(genericSource.CommentGeneratorDisclaimer);

      /* Add includes */
      if (true == createTypesFile)
      {
        includesHeader.AddIncludeFile(moduleName + "_Types.h");
      }
      else
      {
        includesHeader.AddIncludeFile("Std_Types.h");

        /* Add Data type informations */
        doxEnumDescr.AddElementBrief("Available application errors of the " + moduleName);
        doxEnumDescr.AddElementDetails("This enumeration implements all available application return and error values within this specific module. The Application error could be used as DET information or as return type of internal functions.");
        string[] detErrors = { ( moduleName.ToUpper() + "_E_OK = 0" ), ( moduleName.ToUpper() + "_E_NOT_OK = 1" ), ( moduleName.ToUpper() + "_E_PENDING = 2" ), ( moduleName.ToUpper() + "_E_NOT_IMPLEMENTED = 3" ), ( moduleName.ToUpper() + "_E_GENERIC_PROGRAMMING_FAILURE = 4" ) };
        headerTypes.AddEnumerationTypeDefinition(( moduleName + "_ReturnType" ), detErrors, doxEnumDescr);

        doxEnumDescr.AddElementBrief("Available application return values of the " + moduleName);
        doxEnumDescr.AddElementDetails("Redefinition of the " + moduleName + "_ReturnType as error type.");
        headerTypes.AddNewDataTypeDefinition(( moduleName + "_ReturnType" ), ( moduleName + "_ErrorType" ), doxEnumDescr);
      }
      if (true == createAnStaticCfgFile)
      {
        includesHeader.AddIncludeFile(moduleName + "_Cfg.h");
      }
      if (true == createDummyGenFiles)
      {
        includesHeader.AddIncludeFile(moduleName + "_PBCfg.h");
      }
      if (true == createHighLevelIfFile)
      {
        includesHeader.AddIncludeFile(moduleName + "_HiIf.h");
      }
      if (true == createLowLevelIfFile)
      {
        includesHeader.AddIncludeFile(moduleName + "_LoIf.h");
      }
      if (( false == createLowLevelIfFile ) && ( false == createHighLevelIfFile ) && ( true == createGenericIfFile ))
      {
        includesHeader.AddIncludeFile(moduleName + "_If.h");
      }


      /* Add Objects to gen source to generate file */
      genHeader.AddFileName(fileNameHeader);
      genHeader.AddFilePath(basePath + "\\" + moduleName + "\\04_StaticData\\inc");
      genHeader.AddFileGroup(moduleFileGroupOpener, moduleFileGroupCloser);
      genHeader.AddTypesObject(headerTypes);
      genHeader.AddFileCommentHeader(doxHeader);
      genHeader.AddGlobalFunctionPrototypeObject(functions);
      genHeader.AddIncludeObject(includesHeader);

      if (true == createUserCodeSections)
      {
        genHeader.AddUserCodeSections();
      }

      genHeader.GenerateSourceFile();
    }

    /* Generation of types header */
    static private void GenerateTypesFile()
    {
      string fileNameHeader = moduleName + "_Types.h";
      CHeaderFile genHeader = new CHeaderFile();
      DoxygenFileHeader doxHeader = new DoxygenFileHeader();
      CFileTypeObjects headerTypes = new CFileTypeObjects();
      DoxygenElementDescription doxEnumDescr = new DoxygenElementDescription();
      CFileIncludeObjects includesHeader = new CFileIncludeObjects();


      Console.WriteLine("> Start generation of module  types header file: " + fileNameHeader);

      /* Add doxygen Header */
      doxHeader.AddFileAuthor(authorName);
      doxHeader.AddFileBrief("Implementation of module global datatypes from the \"" + moduleName + "\" module.\n");
      doxHeader.AddFileName(fileNameHeader);
      doxHeader.AddFileNote(genericSource.CommentGeneratorDisclaimer);

      /* Add Data type informations */
      doxEnumDescr.AddElementBrief("Available application errors of the " + moduleName);
      doxEnumDescr.AddElementDetails("This enumeration implements all available application return and error values within this specific module. The Application error could be used as DET information or as return type of internal functions.");
      string[] detErrors = { ( moduleName.ToUpper() + "_E_OK = 0" ), ( moduleName.ToUpper() + "_E_NOT_OK = 1" ), ( moduleName.ToUpper() + "_E_PENDING = 2" ), ( moduleName.ToUpper() + "_E_NOT_IMPLEMENTED = 3" ), ( moduleName.ToUpper() + "_E_GENERIC_PROGRAMMING_FAILURE = 4" )};
      headerTypes.AddEnumerationTypeDefinition(( moduleName + "_ReturnType" ), detErrors, doxEnumDescr);

      doxEnumDescr.AddElementBrief("Available application return values of the " + moduleName);
      doxEnumDescr.AddElementDetails("Redefinition of the " + moduleName + "_ReturnType as error type.");
      headerTypes.AddNewDataTypeDefinition(( moduleName + "_ReturnType" ), ( moduleName + "_ErrorType" ), doxEnumDescr);

      /* Add includes */
      includesHeader.AddIncludeFile("Std_Types.h");

      genHeader.AddFileName(fileNameHeader);
      genHeader.AddFilePath(basePath + "\\" + moduleName + "\\04_StaticData\\inc");
      genHeader.AddFileCommentHeader(doxHeader);
      genHeader.AddFileGroup(moduleFileGroupOpener, moduleFileGroupCloser);
      genHeader.AddTypesObject(headerTypes);
      genHeader.AddIncludeObject(includesHeader);
      genHeader.GenerateSourceFile();
    }

    /* Create a memory mapping header file */
    static private void GenerateMemMapFile()
    {
      string tempFileString;
      string[] tempFileStringList;
      string[] stringSeparators = new string[] { "\r\n" };
      int dummylength;
      string filePath = basePath + "\\" + moduleName + "\\03_GenData\\inc\\";
      string fileName = "_" + moduleName + "_MemMap.h";

      Console.WriteLine("> Start generation of TEMPLATE local mem map file: " + fileName);
      
      /* Create Memory Mapping file */
      /* Read template file */
      tempFileString = OsarResources.Generator.Resources.genericSource.memMapFileHeader;
      tempFileString = tempFileString.Replace("<ModuleName>_MemMap.h", moduleName +"_MemMap.h");
      tempFileString = tempFileString.Replace("<ModuleName>", moduleName);
      tempFileString = tempFileString.Replace("<GeneratorName>", authorName);
      tempFileString = tempFileString.Replace("<Date>", DateTime.Now.ToString());
      tempFileStringList = tempFileString.Split(stringSeparators, StringSplitOptions.None);

      /* Check komment length */
      dummylength = tempFileStringList[0].Length;
      tempFileString = "";
      for (int idx = 0; idx < tempFileStringList.Length; idx++)
      {
        if (tempFileStringList[idx].Contains("/* " + moduleName))
        {
          if (tempFileStringList[idx].Length > dummylength)
          {
            tempFileStringList[idx] = tempFileStringList[idx].Remove(( tempFileStringList[idx].Length - 4 ), 1);
          }
          else
          {
            while (tempFileStringList[idx].Length < dummylength)
            {
              tempFileStringList[idx] = tempFileStringList[idx].Insert(( tempFileStringList[idx].Length - 4 ), " ");
            }
          }
        }

        /* Combine string list again*/
        tempFileString = tempFileString + tempFileStringList[idx] + "\r\n";
      }

      /* Create default config file */
      /* Check if file allready exists */
      
      if (File.Exists(filePath + fileName))
      {
        File.Delete(filePath + fileName);   //Delete old file if it allready exists.
      }

      /* Create and open file to write content */
      File.AppendAllText(filePath + fileName, tempFileString);
    }

    /* Create an low level interface header file */
    static private void GeneratLowLevelIfFile()
    {
      string fileNameHeader = moduleName + "_LoIf.h";
      CHeaderFile genHeader = new CHeaderFile();
      DoxygenFileHeader doxHeader = new DoxygenFileHeader();
      CFileIncludeObjects includesHeader = new CFileIncludeObjects();


      /*---------------------------- Create header file -------------------------------*/
      Console.WriteLine("> Start generation of module low level interface header file: " + fileNameHeader);

      /* Setup doxygen file header */
      doxHeader.AddFileAuthor(authorName);
      doxHeader.AddFileBrief("Declaration of "+ moduleName +" Module Mendatroy Low Level Interfaces");
      doxHeader.AddFileDetails("Global" + moduleName + " Low Level Interfaces");
      doxHeader.AddFileVersion("0.1.0");

      /* Add includes */
      
      if (true == createTypesFile)
      {
        includesHeader.AddIncludeFile(moduleName + "_Types.h");
      }
      else
      {
        includesHeader.AddIncludeFile("Std_Types.h");
        includesHeader.AddIncludeFile(moduleName + ".h");
      }
      if (true == createGenericIfFile)
      {
        includesHeader.AddIncludeFile(moduleName + "_If.h");
      }

      genHeader.AddFileName(fileNameHeader);
      genHeader.AddFilePath(basePath + "\\" + moduleName + "\\02_Mandatory\\inc");
      genHeader.AddFileGroup(moduleFileGroupOpener, moduleFileGroupCloser);
      genHeader.AddFileCommentHeader(doxHeader);
      genHeader.AddIncludeObject(includesHeader);
      genHeader.GenerateSourceFile();
    }

    /* Create an high level interface header file */
    static private void GeneratHighLevelIfFile()
    {
      string fileNameHeader = moduleName + "_HiIf.h";
      CHeaderFile genHeader = new CHeaderFile();
      DoxygenFileHeader doxHeader = new DoxygenFileHeader();
      CFileIncludeObjects includesHeader = new CFileIncludeObjects();


      /*---------------------------- Create header file -------------------------------*/
      Console.WriteLine("> Start generation of module high level interface header file: " + fileNameHeader);

      /* Setup doxygen file header */
      doxHeader.AddFileAuthor(authorName);
      doxHeader.AddFileBrief("Declaration of " + moduleName + " Module Mendatroy High Level Interfaces");
      doxHeader.AddFileDetails("Global" + moduleName + " High Level Interfaces and Data Elements.");
      doxHeader.AddFileVersion("0.1.0");

      /* Add includes */
      if (true == createTypesFile)
      {
        includesHeader.AddIncludeFile(moduleName + "_Types.h");
      }
      else
      {
        includesHeader.AddIncludeFile("Std_Types.h");
        includesHeader.AddIncludeFile(moduleName + ".h");
      }
      if (true == createGenericIfFile)
      {
        includesHeader.AddIncludeFile(moduleName + "_If.h");
      }

      genHeader.AddFileName(fileNameHeader);
      genHeader.AddFilePath(basePath + "\\" + moduleName + "\\02_Mandatory\\inc");
      genHeader.AddFileGroup(moduleFileGroupOpener, moduleFileGroupCloser);
      genHeader.AddFileCommentHeader(doxHeader);
      genHeader.AddIncludeObject(includesHeader);
      genHeader.GenerateSourceFile();
    }
    
    /* Create an generic interface header file */
    static private void GeneratGenericIfFile()
    {
      string fileNameHeader = moduleName + "_If.h";
      CHeaderFile genHeader = new CHeaderFile();
      DoxygenFileHeader doxHeader = new DoxygenFileHeader();
      CFileIncludeObjects includesHeader = new CFileIncludeObjects();


      /*---------------------------- Create header file -------------------------------*/
      Console.WriteLine("> Start generation of module generic interface header file: " + fileNameHeader);

      /* Setup doxygen file header */
      doxHeader.AddFileAuthor(authorName);
      doxHeader.AddFileBrief("Declaration of " + moduleName + " Module Mendatroy Generic Interfaces");
      doxHeader.AddFileDetails("Global" + moduleName + " Generic Interfaces and Data Elements.");
      doxHeader.AddFileVersion("0.1.0");

      /* Add includes */
      if (true == createTypesFile)
      {
        includesHeader.AddIncludeFile(moduleName + "_Types.h");
      }
      else
      {
        includesHeader.AddIncludeFile("Std_Types.h");
        includesHeader.AddIncludeFile(moduleName + ".h");
      }

      genHeader.AddFileName(fileNameHeader);
      genHeader.AddFilePath(basePath + "\\" + moduleName + "\\02_Mandatory\\inc");
      genHeader.AddFileGroup(moduleFileGroupOpener, moduleFileGroupCloser);
      genHeader.AddFileCommentHeader(doxHeader);
      genHeader.AddIncludeObject(includesHeader);
      genHeader.GenerateSourceFile();
    }

    /* Generation of a makefile */
    static private void GenerateModuleMakefileFile()
    {
      string fileNameHeader = moduleName + ".mk";
      ModuleMakefile genMakefile = new ModuleMakefile();

      /*---------------------------- Create makefile file -------------------------------*/
      Console.WriteLine("> Start generation of module makefile file.");

      genMakefile.AddFileName(fileNameHeader);
      genMakefile.AddFilePath(basePath + "\\" + moduleName + "\\06_Makefile");
      genMakefile.AddMakefileModuleName(moduleName);
      genMakefile.AddMakefileBasePath(makefileModuleBaseFolder);

      genMakefile.AddMakefileIncludePath("02_Mandatory\\inc");
      genMakefile.AddMakefileIncludePath("03_GenData\\inc");
      genMakefile.AddMakefileIncludePath("04_StaticData\\inc");

      genMakefile.AddMakefileSourceFileName("04_StaticData\\src\\" + moduleName + ".c");

      if (true == createDummyGenFiles)
      {
        genMakefile.AddMakefileSourceFileName("03_GenData\\src\\" + moduleName + "_PBCfg.c");
      }

      /* Generate file */
      genMakefile.GenerateSourceFile();

    }

  }
}
